Project name: 
        
    Sentiment Analysis in Tweets


Members:

	Ciubotariu Calin-Cristian (calin.crist@gmail.com)

	Gliga Mihai               (mihaigliga21@gmail.com)

	Hrisca Marius             (hrisca.marius@gmail.com)




Coordinator:

	Lect. dr. Trandabat Diana


Contact:

	Any of the e-mails above


State of the art:

    https://docs.google.com/document/d/1i5kmMvHVuLfu0Y-8Ye_Yo9FdubEo4wX7Z2ZgpbH7nAY/edit?usp=sharing

Services comparison:

	https://docs.google.com/document/d/1vEKc9klOLzD90lwClja2vRtS5fP-izADX96OAm0JHKU

SentServ:

	http://sentiserv.azurewebsites.net/