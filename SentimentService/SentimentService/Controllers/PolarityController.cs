﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SentimentService.APIs;
using Newtonsoft.Json;
using System.Web.Http.Description;

namespace SentimentService.Controllers
{
    public class PolarityController : ApiController
    {
        /// <summary>
        /// Base request format.
        /// </summary>
        /// <param name="key">Your API key. Contact iuliuvisovan@gmail.com to get one.</param>
        /// <param name="service">Name of wanted service. Currently available: 'alchemy', 'sentiment140', 'sentiwordnet'.</param>
        /// <param name="request">Request body in JSON format. See documentation for example.</param>
        public HttpResponseMessage Post([FromBody]string request, string key = null, string service = "sentiment140")
        {
            if (key != "DSJNI2BDJUP7DBSOKUS72")
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Access denied. Please use ?key=<provided_key> or contact iuliuvisovan@gmail.com to receive one.)");
            else
            {
                var factory = new APIFActory();
                IAPI api;
                if (service == "alchemy")
                    api = factory.GetInstance(ApiType.Alchemy);
                else if (service == "sentiment140")
                    api = factory.GetInstance(ApiType.Sentiment140);
                else if (service == "sentiwordnet")
                    api = factory.GetInstance(ApiType.SentiWordNet);
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Service unsupported. (service=alchemy / service=sentiment140 / service=sentiwordnet)");

                SentimentDto dto;
                try
                {
                    dto = JsonConvert.DeserializeObject<SentimentDto>(request);
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Request was not well-formed JSON! (Make sure you have the 'Content-Type: x-www-form-urlencoded' HTTP header.)");
                }
                try
                {
                    api.SetPolarity(dto);
                    var json = JsonConvert.SerializeObject(dto);
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "Successfully computed polarity.");
                    response.Content = new StringContent(json);
                    return response;
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.MethodNotAllowed,
                        $"Sorry, service '{service}' cannot process your request.\n" +
                        $"Service message: {ex.Message}");
                }
            }
        }
    }
}
