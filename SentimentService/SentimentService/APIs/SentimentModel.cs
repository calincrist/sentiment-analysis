﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SentimentService.APIs
{
    public class SentimentModel
    {
        public string text { get; set; }
        public int polarity { get; set; }
    }
}