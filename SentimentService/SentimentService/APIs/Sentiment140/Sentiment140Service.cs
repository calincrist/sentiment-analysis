﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlchemyAPI;
using System.Xml.Linq;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace SentimentService.APIs.Sentiment140
{
    public class Sentiment140Service : IAPI
    {
        public void SetPolarity(SentimentDto dto)
        {
            var webAddr = "http://www.sentiment140.com/api/bulkClassifyJson?appid="
                + File.ReadAllText(HttpContext.Current.Server.MapPath("~/APIs/Sentiment140/api_key.txt"));
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(dto);
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                var obj = JsonConvert.DeserializeObject<SentimentDto>(result);
                dto.data.Clear();
                foreach (var item in obj.data)
                {
                    item.polarity -= 2;
                    dto.data.Add(item);
                }
            }
        }
    }
}