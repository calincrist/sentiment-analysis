﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlchemyAPI;
using System.Xml.Linq;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace SentimentService.APIs.SentiWordNet
{
    public class SentiWordNetService : IAPI
    {
        public void SetPolarity(SentimentDto dto)
        {
            if (dto.data.Any(x => x.text.Any(Char.IsSeparator) || x.text.Any(Char.IsSymbol)))
                throw new Exception("Please supply single words in object 'text' field. SentiWordNet service works with words, not sentences.");
            var db = LoadDict();
            foreach (var item in dto.data)
            {
                var dbTerm = db.FirstOrDefault(x => x.Term == item.text.ToLower());
                item.polarity = Convert.ToInt32((dbTerm.PosScore - dbTerm.NegScore) * 2);
            }
        }

        private List<SynSet> LoadDict()
        {
            Stream stream = new FileStream(
                HttpContext.Current.Server.MapPath("~/APIs/SentiWordNet/Dict.bin"),
                FileMode.Open,
                FileAccess.Read,
                FileShare.None);
            IFormatter formatter = new BinaryFormatter();
            var db = (List<SynSet>)formatter.Deserialize(stream);
            stream.Close();
            return db;
        }
    }
}