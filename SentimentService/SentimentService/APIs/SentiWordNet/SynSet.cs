﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SentimentService.APIs.SentiWordNet
{
    [Serializable]
    public class SynSet
    {
        public float PosScore { get; set; }
        public float NegScore { get; set; }
        public string Term { get; set; }
    }
}