﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SentimentService.APIs
{
    public class SentimentDto
    {
        public List<SentimentModel> data { get; set; }
    }
}