﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlchemyAPI;
using System.Xml.Linq;

namespace SentimentService.APIs.Alchemy
{
    public class AlchemyService : IAPI
    {
        public void SetPolarity(SentimentDto dto)
        {
            // Create an AlchemyAPI object.
            var alchemyObj = new AlchemyAPI.AlchemyAPI();

            // Load an API key from disk.
            alchemyObj.LoadAPIKey(HttpContext.Current.Server.MapPath("~/APIs/Alchemy/api_key.txt"));

            // Extract sentiment from a text string.
            if (dto.data.Count > 500)
                throw new Exception("Maximum of 500 sentences / request!");
            foreach (var item in dto.data)
            {
                var xml = alchemyObj.TextGetTextSentiment(item.text);
                XDocument doc = XDocument.Parse(xml);
                var type = (from t in doc.Descendants("type") select t).FirstOrDefault().Value;
                if (type == "neutral")
                    item.polarity = 0;
                else
                {
                    var alchemyPolarity = Convert.ToDouble((from t in doc.Descendants("score") select t).FirstOrDefault().Value);
                    var sentiPolarity = Convert.ToInt32(alchemyPolarity * 2);
                    item.polarity = sentiPolarity;
                }
            }

        }
    }
}