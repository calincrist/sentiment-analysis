﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SentimentService.APIs
{
    public enum ApiType { Alchemy, Sentiment140, SentiWordNet }
    public class APIFActory
    {
        public IAPI GetInstance(ApiType apiType)
        {
            if (apiType == ApiType.Alchemy)
                return new Alchemy.AlchemyService();
            else if (apiType == ApiType.Sentiment140)
                return new Sentiment140.Sentiment140Service();
            else
                return new SentiWordNet.SentiWordNetService();
        }
    }
}