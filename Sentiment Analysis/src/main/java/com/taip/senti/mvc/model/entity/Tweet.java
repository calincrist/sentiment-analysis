package com.taip.senti.mvc.model.entity;

/**
 * Created by calincrist on 10.11.2015.
 */
public class Tweet {
    private int id;
    private String tweeterId;
    private String body;

    public void setId(int id) {
        this.id = id;
    }

    public void setTweeterId(String tweeterId) {
        this.tweeterId = tweeterId;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public String getTweeterId() {
        return tweeterId;
    }

    public String getBody() {
        return body;
    }
}
