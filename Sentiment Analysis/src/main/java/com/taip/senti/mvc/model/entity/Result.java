package com.taip.senti.mvc.model.entity;

/**
 * Created by calincrist on 10.11.2015.
 */
public class Result {

    private int id;
    private Tweet tweet;
    private String sentiment;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Tweet getTweet() {
        return tweet;
    }

    public void setTweet(Tweet tweet) {
        this.tweet = tweet;
    }

    public String getSentiment() {
        return sentiment;
    }

    public void setSentiment(String sentiment) {
        this.sentiment = sentiment;
    }
}
