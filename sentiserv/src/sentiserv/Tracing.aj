package sentiserv;

aspect Tracing {
	 private int callDepth = -1;
	 
	 pointcut tracePoint(): !within(Tracing);
	 
	 before() : tracePoint() {
	 callDepth++; print("Before", thisJoinPoint);
	 }
	 after() : tracePoint() {
	 print("After ", thisJoinPoint); callDepth--;
	 }
	 private void print(String prefix, Object message) {
	 for(int i = 0, spaces = callDepth * 2; i < spaces; i++)
	 System.out.print(" ");
	 System.out.println(prefix + ": " + message);
	 }
	}