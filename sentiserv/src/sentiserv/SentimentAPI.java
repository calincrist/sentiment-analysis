package sentiserv;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class SentimentAPI {
	
	String startInput  = "={\"data\":[";
	String startElement = "{\"text\": \"";
	String endElement = "\"}";
	String endInput = "]}";

	public abstract String getPolarity(String tweet) throws IOException;
	
	 String callService(String url, String parameters) throws IOException{
			URL obj = new URL(url);
	        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
	        conn.setRequestMethod("POST");
	        conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(parameters);
			wr.flush();
			wr.close();
			
			int responseCode = conn.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("\nPost parameters : " + parameters);
			System.out.println("\nResponse Code : " + responseCode+"\n");

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(conn.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return response.toString();
		}
}
