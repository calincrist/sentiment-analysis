package sentiserv;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
//		Sentiserv sentiserv = new Sentiserv();
//		Map<String, String> result;
//		result = sentiserv.getPolarity();
		
		
		SentimentAPI alchemy = new AlchemyAPI();
		SentimentAPI sent140 = new Sentiment140API();
		
		System.out.println("Result Alchemy " +alchemy.getPolarity("I love you")+"\n");
		System.out.println("Result Sentiment140 " +sent140.getPolarity("I love you")+"\n");
		
//		System.out.println("Result Alchemy " + result.get("alchemy")+"\n");
//		System.out.println("Result Sentiment140 " + result.get("sentiment140")+"\n");
	}
}
