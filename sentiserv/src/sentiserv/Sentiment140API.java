package sentiserv;

import java.io.IOException;

public class Sentiment140API extends SentimentAPI {

	String urlSentiment140 = "http://sentiserv.azurewebsites.net/api/Polarity?key=DSJNI2BDJUP7DBSOKUS72&service=sentiment140";
	
	@Override
	public String getPolarity(String tweet) throws IOException {
		String parameters;
		parameters = startInput + startElement + tweet + endElement + endInput;
		return super.callService(urlSentiment140, parameters);
	}

}
