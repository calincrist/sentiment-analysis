package getTweetsPack;

import twitter4j.*;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class GetTweets {

	private static final String CONSUMER_KEY		= "R4dICN9z1B9wAl9JiVne2yMtD";
	private static final String CONSUMER_SECRET 	= "HwbZCGrAonAyEkNbMyeQPqsNoqweuj4F7I5goXu79CDl9CKmqV";

	public static String cleanText(String text)
	{
		text = text.replace("\n", "\\n");
		text = text.replace("\t", "\\t");

		return text;
	}
	
	public static OAuth2Token getOAuth2Token()
	{
		OAuth2Token token = null;
		ConfigurationBuilder cb;

		cb = new ConfigurationBuilder();
		cb.setApplicationOnlyAuthEnabled(true);

		cb.setOAuthConsumerKey(CONSUMER_KEY).setOAuthConsumerSecret(CONSUMER_SECRET);

		try
		{
			token = new TwitterFactory(cb.build()).getInstance().getOAuth2Token();
		}
		catch (Exception e)
		{
			System.out.println("Could not get OAuth2 token");
			e.printStackTrace();
			System.exit(0);
		}

		return token;
	}
	
	public static Twitter getTwitter()
	{
		OAuth2Token token;

		token = getOAuth2Token();

		
		ConfigurationBuilder cb = new ConfigurationBuilder();

		cb.setApplicationOnlyAuthEnabled(true);

		cb.setOAuthConsumerKey(CONSUMER_KEY);
		cb.setOAuthConsumerSecret(CONSUMER_SECRET);

		cb.setOAuth2TokenType(token.getTokenType());
		cb.setOAuth2AccessToken(token.getAccessToken());

		//	And create the Twitter object!
		return new TwitterFactory(cb.build()).getInstance();

	}
	
	public static void sleep(int time){
		try{
			Thread.sleep(time);
		}catch(Exception e){
			
		}
	}
	    	
	public String GET (String[] args) throws IOException, TwitterException, InterruptedException{
		
		Twitter twitter = getTwitter();		
		
	    Map<String, RateLimitStatus> rateLimitStatus = twitter.getRateLimitStatus();
	    RateLimitStatus searchTweetsRateLimit = rateLimitStatus.get("/statuses/show/:id");
	   
	    System.out.printf("You have %d calls remaining out of %d, Limit resets in %d seconds\n",
				  searchTweetsRateLimit.getRemaining(),
				  searchTweetsRateLimit.getLimit(),
				  searchTweetsRateLimit.getSecondsUntilReset());
	        	
		  // Open the files
			try{
			    FileInputStream fstream = new FileInputStream("input.txt");
			    BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

			    FileWriter fwriter = new FileWriter("output.txt", false);
				BufferedWriter bw = new BufferedWriter(fwriter);
				
			    String strLine;				    				         
	
				  //Read File Line By Line
				  while ((strLine = br.readLine()) != null)   {						  
  
						  	try {									  
									  Status statusList = twitter.showStatus(Long.parseLong(strLine));
		         					  String tw = statusList.getText();         					  
		         						  System.out.println("Tweet has been written into output file!!!");
		         						  for (int i=0; i<TwitterResponse.READ; i++) {								   								  
		         							  bw.write("");								  
		         							  bw.write(cleanText(tw)+"\n");	
		         						  }							    	
							    	
						        } catch (NumberFormatException e) {
						            e.printStackTrace();
						        } catch (TwitterException e) {				            
						        	//System.out.println(e);
						        	System.out.println("Error retrieving this tweet :(");
						        	bw.write("This tweet has not been found!!" +"\n");
						        }						    						  							  					  			   					    	      	    		    					  
				  }
				//close files
				  br.close();
				  bw.close();
				}
				catch(FileNotFoundException e)
				{
					System.out.println(e);
				} 			
		return null;	
		}	 
}
